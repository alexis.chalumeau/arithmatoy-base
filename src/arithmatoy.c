#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

int VERBOSE = 0;

const char *get_all_digits() { return "0123456789abcdefghijklmnopqrstuvwxyz"; }
const size_t ALL_DIGIT_COUNT = 36;

void arithmatoy_free(char *number) { free(number); }

unsigned int get_max_length(const char *lhs, const char *rhs){
	unsigned int lhs_size = strlen(lhs);
	unsigned int rhs_size = strlen(rhs);

	if (lhs_size > rhs_size)
	{
		return lhs_size;
	}
	else
	{
		return rhs_size;
	}
}

void add_final_carry(char *number){
	unsigned int size_number = strlen(number);
	number = realloc(number, size_number + 1);
	number[size_number] = to_digit(1);
}

char *arithmatoy_add(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "add: entering function\n");
  }

  // Fill the function, the goal is to compute lhs + rhs
  // You should allocate a new char* large enough to store the result as a
  // string Implement the algorithm Return the result

 	//Get max length between lhs and rhs
  	unsigned int size_result = get_max_length(lhs, rhs);

	unsigned int lhs_size = strlen(lhs);
	unsigned int rhs_size = strlen(rhs);

	char * first = malloc(lhs_size+1);
	strcpy(first,lhs);
	char * second = malloc(rhs_size+1);
	strcpy(second,rhs);

	first[lhs_size] = '\0';
	second[rhs_size] = '\0';

	reverse(first);
	reverse(second);

	char *result = malloc(size_result+1);

	unsigned int carry = 0;

	for (int i = 0; i < size_result; i++)
	{
		unsigned int lhs_val = 0;
		unsigned int rhs_val = 0;
		char lhs_digit = '0';
		char rhs_digit = '0';


		if(i < lhs_size){
			lhs_digit = first[i];
			lhs_val = get_digit_value(first[i]);
		}
		if(i < rhs_size){
			rhs_digit = second[i];
			rhs_val = get_digit_value(second[i]);
		}

		if (VERBOSE){
			fprintf(stderr, "add: digit %c digit %c carry %c\n", lhs_digit, rhs_digit, to_digit(carry));
		}

		unsigned int result_val = lhs_val + rhs_val + carry;

		if (result_val >= base){
			result_val -= base;
			carry = 1;
		}else{
			carry = 0;
		}

		if (VERBOSE){
			fprintf(stderr, "add: result: digit %c carry %c\n", to_digit(result_val), to_digit(carry));
		}

		char result_digit = to_digit(result_val);
		result[i] = result_digit;

		if (i  == size_result-1 && carry == 1){
			if (VERBOSE){
				fprintf(stderr, "add: final carry 1\n");
			}
			add_final_carry(result);
			size_result += 1;
			break;
		}	
	}
	result[size_result] = '\0';
	reverse(result);
	result = drop_leading_zeros(result);
	return result;
}

char *arithmatoy_sub(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "sub: entering function\n");
  }

  // Fill the function, the goal is to compute lhs - rhs (assuming lhs > rhs)
  // You should allocate a new char* large enough to store the result as a
  // string Implement the algorithm Return the result

	unsigned int size_result = get_max_length(lhs,rhs);

	char * result = malloc(size_result);


	unsigned int lhs_size = strlen(lhs);
	unsigned int rhs_size = strlen(rhs);

	char * first = malloc(lhs_size);
	strcpy(first,lhs);
	reverse(first);
	char * second = malloc(rhs_size);
	strcpy(second,rhs);
	reverse(second);

	unsigned int carry = 0;

	for(int i = 0; i < size_result; i++){
		int lhs_val = 0;
		int rhs_val = 0;
		char lhs_digit = '0';
		char rhs_digit = '0';

		if(i < lhs_size){
			lhs_digit = first[i];
			lhs_val = get_digit_value(first[i]);
		}
		if( i < rhs_size){
			rhs_digit = second[i];
			rhs_val = get_digit_value(second[i]);
		}

		if (VERBOSE){
			fprintf(stderr,"sub: digit %c digit %c carry %c\n", lhs_digit, rhs_digit,to_digit(carry));
		}

		int result_val = 0;
		int temp_result = lhs_val-(carry)-rhs_val;
		if (temp_result < 0){
			result_val = temp_result + base;
			carry = 1;
		}else{
			result_val = temp_result;
			carry = 0;
		}
		
		char digit_result = to_digit(result_val);

		if (VERBOSE){
			fprintf(stderr,"sub: result: digit %c carry %c\n",digit_result,to_digit(carry)); 
		}
		result[i] = digit_result;

		if (i+1 == size_result && carry == 1){
			return NULL;
		}
	}
	result[size_result] = '\0';
	reverse(result);
	result = drop_leading_zeros(result);
	return result;

}

void add_zeros(char * result, int n){
	for(int i=0;i<n;i++){
		result[i] = '0';
	}
}

void add_final_carry_n(char * result, int result_size, int carry){
	result = realloc(result, result_size+2);
	result[result_size] = to_digit(carry);
	result[result_size+1] = '\0';
}

char *arithmatoy_mul(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "mul: entering function\n");
  }

  // Fill the function, the goal is to compute lhs * rhs
  // You should allocate a new char* large enough to store the result as a
  // string Implement the algorithm Return the result
	unsigned int lhs_size = strlen(lhs);
	unsigned int rhs_size = strlen(rhs);
  	
  	unsigned int size_result = (lhs_size * rhs_size) - 1;

	char * result = malloc(size_result);
	memset(result,'0',size_result);
	result[size_result] = '\0';

	char * first = malloc(lhs_size);
	strcpy(first, lhs);
	reverse(first);
	char * second = malloc(rhs_size);
	strcpy(second, rhs);
	reverse(second);


	for (int i=0;i < rhs_size;i++){
		int carry = 0;

		int rhs_val = get_digit_value(second[i]);
	
		if (VERBOSE){
			fprintf(stderr, "mul: digit %c number %s\n",second[i],lhs);
		}

		char * sub_result = malloc(lhs_size+i);
		add_zeros(sub_result,i);
		for (int j=0; j < lhs_size;j++){
			int lhs_val = get_digit_value(first[j]);
			
			if (VERBOSE){
				fprintf(stderr,"mul: digit %c digit %c carry %c\n",second[i],first[j],to_digit(carry));
			}

			int result_val = rhs_val * lhs_val+carry;
			if (result >= base){
				carry = result_val / base;
				result_val = result_val % base;
			}

			if (VERBOSE){
				fprintf(stderr,"mul: result: digit %c carry %c\n",to_digit(result_val),to_digit(carry));
			}

			sub_result[j+i] = to_digit(result_val);

			if (j+1 == lhs_size && carry > 0){
				add_final_carry_n(sub_result, lhs_size+i , carry);
			}else if (j+1 == lhs_size){
				sub_result[lhs_size+i] = '\0';
			}
		}
		reverse(sub_result);
		
		if (VERBOSE){
			fprintf(stderr,"mul: add %s + %s\n",result,sub_result);
		}

		result = arithmatoy_add(base,result,sub_result);
		free(sub_result);

		if (VERBOSE){
			fprintf(stderr,"mul: result: %s\n",result);
		}

	}
	return result;
}

// Here are some utility functions that might be helpful to implement add, sub
// and mul:

unsigned int get_digit_value(char digit) {
  // Convert a digit from get_all_digits() to its integer value
  if (digit >= '0' && digit <= '9') {
    return digit - '0';
  }
  if (digit >= 'a' && digit <= 'z') {
    return 10 + (digit - 'a');
  }
  return -1;
}

char to_digit(unsigned int value) {
  // Convert an integer value to a digit from get_all_digits()
  if (value >= ALL_DIGIT_COUNT) {
    debug_abort("Invalid value for to_digit()");
    return 0;
  }
  return get_all_digits()[value];
}

char *reverse(char *str) {
  // Reverse a string in place, return the pointer for convenience
  // Might be helpful if you fill your char* buffer from left to right
  const size_t length = strlen(str);
  const size_t bound = length / 2;
  for (size_t i = 0; i < bound; ++i) {
    char tmp = str[i];
    const size_t mirror = length - i - 1;
    str[i] = str[mirror];
    str[mirror] = tmp;
  }
  return str;
}

const char *drop_leading_zeros(const char *number) {
  // If the number has leading zeros, return a pointer past these zeros
  // Might be helpful to avoid computing a result with leading zeros
  if (*number == '\0') {
    return number;
  }
  while (*number == '0') {
    ++number;
  }
  if (*number == '\0') {
    --number;
  }
  return number;
}

void debug_abort(const char *debug_msg) {
  // Print a message and exit
  fprintf(stderr, debug_msg);
  exit(EXIT_FAILURE);
}
