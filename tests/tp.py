# Aucun n'import ne doit être fait dans ce fichier


def nombre_entier(n: int) -> str:
    result = ""
    for i in range(n):
        result += 'S'
    result += '0'
    return result


def S(n: str) -> str:
    return 'S'+n


def addition(a: str, b: str) -> str:
    if a=="0":
        return b
    if a.startswith("S"):
        return S(addition(a[1:], b))


def multiplication(a: str, b: str) -> str:
    if a=="0":
        return a
    if a.startswith("S"):
        return addition(multiplication(a[1:],b),b)

def facto_ite(n: int) -> int:
    if n==0:
        return 1
    result = 1
    for i in range(1,n+1):
        result *= i
    return result


def facto_rec(n: int) -> int:
    if n==0:
        return 1
    return facto_rec(n-1)*n


def fibo_rec(n: int) -> int:
    if n == 0:
        return 0
    if n == 1:
        return 1
    if n > 1:
        return fibo_rec(n-1) + fibo_rec(n-2)
    return None

def fibo_ite(n: int) -> int:
    if n == 0:
        return 0
    if n == 1:
        return 1
    if n > 1:
        result = 1
        step = [0,1]
        for i in range(2,n+1):
            result = step[len(step)-1] + step[len(step)-2]
            step.append(result)
        return result

def golden_phi(n: int) -> int:
    return fibo_rec(n+1)/fibo_rec(n)


def sqrt5(n: int) -> int:
    return (golden_phi(n)-(1/2))*2


def pow(a: float, n: int) -> float:
    if n == 0:
        return 1;
    if n%2 == 0:
        return pow(a*a,n/2);
    if n%2 == 1:
        return pow(a*a,((n-1)/2));
